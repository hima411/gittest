<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Chemical Equipment And Processing</description>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>ChemicalEquipmentAndProcessing</label>
    <tabs>APEX_Customer__c</tabs>
    <tabs>APEX_Invoice__c</tabs>
</CustomApplication>
